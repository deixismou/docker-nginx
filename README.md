[![pipeline status](https://gitlab.com/deixismou/docker-nginx/badges/master/pipeline.svg)](https://gitlab.com/deixismou/docker-nginx/commits/master)

![Nginx](logo.png)

# About this Repo

This is a fork of the official Docker image for [Nginx]. It provides minor fixes
to address an issue in the official Alpine Linux variant which make it unusable
with php-fpm containers. The upstream maintainers [have clearly stated] that
they have no intention of changing this in the official versions.

Specifically, while Debian provides user www-data (33) and group www-data (33),
nginx:alpine provides user xfs (33) and group xfs (33); fpm-alpine expects user
www-data (82) and group www-data (82). If this user and group do not match or
are missing, it causes Nginx to be denied access to the php-fpm socket. This
image simply adds the appropriate user and group during the docker build,
allowing Nginx and php-fpm to function normally.

The non-Alpine images are also included here, in order to remain consistent with
the official repo. 

[Nginx]:https://registry.hub.docker.com/_/nginx/
[have clearly stated]:https://github.com/nginxinc/docker-nginx/issues/135

# What is Nginx?

Nginx (pronounced "engine-x") is an open source reverse proxy server for HTTP,
HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer, HTTP cache,
and a web server (origin server). The Nginx project started with a strong focus
on high concurrency, high performance and low memory usage. It is licensed under
the 2-clause BSD-like license and it runs on Linux, BSD variants, Mac OS X,
Solaris, AIX, HP-UX, as well as on other \*nix flavors. It also has a proof of
concept port for Microsoft Windows.

> [wikipedia.org/wiki/Nginx](https://en.wikipedia.org/wiki/Nginx)

# How to use this image

This image should be identical to the official images in every other respect,
therefore instructions for usage remain the same. The full readme can be
found in [official docs repo] or on the [Docker Hub page].

[official docs repo]:https://github.com/docker-library/docs/tree/master/nginx
[Docker Hub page]:https://hub.docker.com/_/nginx/
